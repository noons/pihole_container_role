Pihole Container
================

Setting up Pihole with Podman in rootless mode

Requirements
------------
- [podman](https://podman.io/)

Role Variables
--------------

- __user_name__: `"pihole"` user to connect when create the container
- __user_group__: `"{{ user_name }}"`
- __pihole_base_path__: `"/home/{{ user_name }}"`
- __tz__: `UTC`
- __virtual_host__: `pi.hole`
- __hostname__: `pi.hole`
- __dns_fallback__: `1.0.0.1`
- __dns1__: `1.1.1.1`
- __dns2__: `8.8.8.8`
- __local_domain__: `lan`
- __query_logging__: `"false"`
- __dnssec__: `"false"`
- __dns_check__: `fsf.org`
- __restart__: `"false"`
- __rev_server__: `"false"`
- __rev_server_target__: `"192.168.1.1"`
- __rev_server_cidr__: `"192.168.0.0/24"`
- __rev_server_domain__: `"lan"`

Dependencies
------------

1. __collections__:
    - `containers.podman`

Example Playbooks
-----------------

```yaml
- hosts: "{{ target | default('none') }}"
  name: Pihole Container Environment
  become: true
  roles:
    - role: pihole_container_role
      vars:
        set_environment: true

- hosts: "{{ target | default('none') }}"
  name: Pihole Container
  roles:
    - role: pihole_container_role
      vars:
        tz: UTC
        virtual_host: pi.hole
        hostname: pi.hole
        local_domain: lan
        dns_fallback: 1.0.0.1
        dns1: 1.1.1.1
        dns2: 8.8.8.8
        query_logging: "true"
        restart: "false"
        rev_server: "true"
        rev_server_target: "192.168.1.1"
        rev_server_cidr: "192.168.0.0/16"
        rev_server_domain: "lan"
```

```yaml
- hosts: "{{ target | default('none') }}"
  name: Pihole Container Environment
  become: true
  roles:
    - role: pihole_container_role
      vars:
        set_environment: true

- hosts: "{{ target | default('none') }}"
  name: Pihole Container
  remote_user: "{{ user_name | default('pihole') }}"
  roles:
    - role: pihole_container_role
      vars:
        user_name: "{{ user_name | default('pihole') }}"
        tz: UTC
        virtual_host: pi.hole
        hostname: pi.hole
        local_domain: lan
        dns_fallback: 1.0.0.1
        dns1: 1.1.1.1
        dns2: 8.8.8.8
        query_logging: "true"
        restart: "false"
        rev_server: "true"
        rev_server_target: "192.168.1.1"
        rev_server_cidr: "192.168.0.0/16"
        rev_server_domain: "lan"
```

License
-------

[GPL-3.0-or-later](./LICENSE)

Author Information
------------------

- [@noons](https://layer8.space/@noons)
- [codeberg](https://codeberg.org/noons)
- [github](https://github.com/noonsleeper)
